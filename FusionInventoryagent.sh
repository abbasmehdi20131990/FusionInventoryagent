#!bin/bash/
#install the FusionInventory agent
##https://documentation.fusioninventory.org/%20FusionInventory_agent/%20%20%20Installation/linux/deb/
##installation des dependances
sudo apt update
sudo apt install -y dmidecode hwdata ucf hdparm perl libuniversal-require-perl libwww-perl libparse-edid-perl libproc-daemon-perl libfile-which-perl libhttp-daemon-perl libxml-treepp-perl libyaml-perl libnet-cups-perl libnet-ip-perl libdigest-sha-perl libsocket-getaddrinfo-perl libtext-template-perl libxml-xpath-perl libyaml-tiny-perl
##telechargement du git
wget https://github.com/fusioninventory/fusioninventory-agent/releases/download/2.6/fusioninventory-agent_2.6-1_all.deb
sudo dpkg -i fusioninventory*.deb
##Configuration du FusionInventory agent
echo "pour configurer ,rajouter l'adresse du serveur GLPI >>sudo nano /etc/fusioninventory/agent.cfg"
sudo systemctl restart fusioninventory-agent
sudo pkill -USR1 -f -P 1 fusioninventory-agent
###Verification dans GLPI
echo "Voir dans GLPI"
sudo fusioninventory-agent
