#!/bin/bash/
##installation des dependences 
sudo apt update 
sudo apt -y install dmidecode hwdata ucf hdparm
sudo apt -y install perl libuniversal-require-perl libwww-perl libparse-edid-perl
sudo apt -y install libproc-daemon-perl libfile-which-perl libhttp-daemon-perl
sudo apt -y install libxml-treepp-perl libyaml-perl libnet-cups-perl libnet-ip-perl
sudo apt -y install libdigest-sha-perl libsocket-getaddrinfo-perl libtext-template-perl
sudo apt -y install libxml-xpath-perl libyaml-tiny-perl

sudo apt -y install libnet-snmp-perl libcrypt-des-perl libnet-nbname-perl
sudo apt -y install libdigest-hmac-perl
sudo apt -y install libfile-copy-recursive-perl libparallel-forkmanager-perl
sudo apt -y install libwrite-net-perl
wget https://github.com/fusioninventory/fusioninventory-agent/releases/download/2.5.2/fusioninventory-agent_2.5.2-1_all.deb
sudo dpkg -i fusioninventory*.deb
sudo systemctl restart fusioninventory-agent
sudo pkill -USR1 -f -P 1 fusioninventory-agent
###Verification dans GLPI
echo "Voir dans GLPI  ou faire le test  http://localhost:62354  "
sudo systemctl status fusioninventory-agent
